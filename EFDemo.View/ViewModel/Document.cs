﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.View.ViewModel
{
    public class Document
    {
        public User Owner { get; set; }

        public string Name { get; set; }
        public string Text { get; set; }
    }
}
