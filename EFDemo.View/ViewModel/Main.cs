﻿using EFDemo.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlTypes;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace EFDemo.View.ViewModel
{
    public class Main
    {
        private IContainer Container { get; set; }

        public User NewUser { get; set; }

        public Document NewDocument { get; set; }

        public long SelectedUserId { get; set; }

        public ObservableCollection<User> AllUsers { get; } = new ObservableCollection<User>();

        public ObservableCollection<Document> SelectedUsersDocuments { get; } = new ObservableCollection<Document>();

        private RelayCommand _createNewUserCommand;

        public RelayCommand CreateNewUserCommand
        {
            get
            {
                return _createNewUserCommand ?? (_createNewUserCommand = new RelayCommand(p => true,
                    p => { CreateNewUser(); }));
            }
        }

        private RelayCommand _createDocumentCommand;

        public RelayCommand CreateDocumentCommand
        {
            get
            {
                return _createDocumentCommand ?? (_createDocumentCommand = new RelayCommand(p => true,
                    p => { CreateDocument(); }));
            }
        }

        private UserService UserService
        {
            get
            {
                using (var scope = Container.BeginLifetimeScope())
                {
                    return scope.Resolve<UserService>();
                }
            }
        }

        private DocumentService DocumentService
        {
            get
            {
                using (var scope = Container.BeginLifetimeScope())
                {
                    return scope.Resolve<DocumentService>();
                }
            }
        }



        /// <summary>
        /// ONLY Design time constructor
        /// </summary>
        public Main()
        {
            this.NewUser = new User(0) { FirstName = "FirstName", LastName = "LastName" };
        }

        public Main(IContainer container)
        {
            Contract.Assert(container != null);

            Container = container;
            NewUser = new User(0);
            NewDocument = new Document();
            GetAllUsers();
        }

        private void CreateNewUser()
        {
            UserService.CreateUser(new Model.User { FirstName = NewUser.FirstName, LastName = NewUser.LastName });
            GetAllUsers();
        }

        private void GetAllUsers()
        {
            AllUsers.Clear();
            foreach (var user in UserService.AllUsers().Select(u => new User(u.ID) { FirstName = u.FirstName, LastName = u.LastName }))
            {
                AllUsers.Add(user);
            }
        }

        private void CreateDocument()
        {
            DocumentService.CreateDocument(new Model.Document() {OwnerId  = SelectedUserId, Name = NewDocument.Name, Text = NewDocument.Text});
            UpdateSelectedUsersDocuments();
        }

        public void UpdateSelectedUsersDocuments()
        {
            SelectedUsersDocuments.Clear();
            foreach (var doc in DocumentService.GetUsersDocuments(SelectedUserId))
            {
                SelectedUsersDocuments.Add(new Document() { Name = doc.Name, Owner = new User(doc.OwnerId) { FirstName = doc.Owner.FirstName, LastName = doc.Owner.LastName } });
            }
        }
    }
}
