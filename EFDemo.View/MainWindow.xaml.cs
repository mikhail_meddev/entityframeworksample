﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EFDemo.Model;
using EFDemo.View.ViewModel;
using Autofac;
using Autofac.Core;

namespace EFDemo.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static IContainer Container { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            RegisterServices();

            DataContext = new Main(Container);
        }

        private static void RegisterServices()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(new UserService()).As<UserService>();
            builder.RegisterInstance(new DocumentService()).As<DocumentService>();
            Container = builder.Build();
        }

        private void UsersListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.UsersListBox.SelectedItems.Count > 0)
            {
                ((ViewModel.Main) DataContext).SelectedUserId = ((ViewModel.User) this.UsersListBox.SelectedItems[0]).Id;
                ((ViewModel.Main) DataContext).UpdateSelectedUsersDocuments();
            }
        }
    }
}
