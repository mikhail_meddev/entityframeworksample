﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.Model
{
    public class DocumentService
    {
        public void CreateDocument(Document doc)
        {
            using (var context = new ModelContext())
            {
                context.Documents.Add(doc);
                context.SaveChanges();
            }
        }

        public IEnumerable<Document> GetUsersDocuments(long userId)
        {
            using (var context = new ModelContext())
            {
                return context.Documents.Include("Owner").Where(doc => doc.OwnerId == userId).ToArray();
            }
        }
    }
}
