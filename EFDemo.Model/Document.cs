﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.Model
{
    public class Document
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public string Text { get; set; }
        public long OwnerId { get; set; }
        //public long FolderId { get; set; }

        public virtual User Owner { get; set; }
        //public virtual Folder Folder { get; set; }
    }
}
