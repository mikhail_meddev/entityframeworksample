﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace EFDemo.Model
{
    public class ModelContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Document> Documents { get; set; }
        //public DbSet<Document> DocumentsTemp { get; set; }
        public DbSet<Folder> Folders { get; set; }

        public ModelContext()
            : base("DbConnection")
        {
            Console.WriteLine("ModelContext created");
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
#if DEBUG
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ModelContext>());
#else
            Database.SetInitializer<ModelContext>(null);
#endif

            modelBuilder.Entity<Document>().Property(d => d.Text).IsUnicode().IsRequired();
        }
    }
}
