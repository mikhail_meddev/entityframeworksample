﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.Model
{
    public class UserService
    {
        public void CreateUser(User user)
        {
            using (var context = new ModelContext())
            {
                context.Users.Add(user);
                context.SaveChanges();
            }
        }

        public IEnumerable<User> AllUsers()
        {
            using (var context = new ModelContext())
            {
                return context.Users.ToArray();
            }
        }

        public User GetUserById(long id)
        {
            using (var context = new ModelContext())
            {
                return context.Users.First(u => u.ID == id);
            }
        }
    }
}
