﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.Model
{
    public class Folder
    {
        public long ID { get; set; }
        public long ParentFolderID { get; set; }

        virtual public Folder ParentFolder { get; set; }
    }
}
